import java.util.Scanner;

/**
 * Created by frans on 25/06/2016.
 */
public class Collatz
{

    static int collatz(int num)
    {
        int steps = 0;

        while(num != 1)
        {
            if(num % 2 == 0)
                num /= 2;

            else
                num = 3 * num + 1;

            steps += 1;
        }

        return steps;
    }

    public static void main(String[] args)
    {
        System.out.print("Enter a number! ");
        int num = new Scanner(System.in).nextInt();
        System.out.println(collatz(num));
    }
}
