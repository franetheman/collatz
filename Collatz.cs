﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collatz
{
    class Collatz
    {
        int collatz(int num)
        {
            int steps = 0;

            while(num != 1)
            {
                if (num % 2 == 0)
                    num /= 2;

                else
                    num = 3 * num + 1;

                steps++;
            }

            return steps;
        }

    }
}
