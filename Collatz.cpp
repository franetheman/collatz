/*
 * Collatz.cpp
 *
 *  Created on: 25 Jun 2016
 *      Author: frans
 */


#include <iostream>

using namespace std;

int collatz(int num);

int main()
{

	int num;

	cout << "Enter a number! " << flush;

	cin >> num;

	cout << collatz(num) << endl;

	return 0;
}

int collatz(int num)
{
	int steps = 0;

	while(num != 1)
	{
		if(num % 2 == 0)
			num /= 2;

		else
			num = 3 * num + 1;

		steps++;
	}

	return steps;
}

