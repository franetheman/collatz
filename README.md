# Collatz #

Collatz conjecture is a mathematical method which will always reach 1.

Collatz conjecture is also known as the "3n +1 conjecture"

### How it works ###

This is how collatz conjecture is calculated.

1. Enter any positive value, the value is referred to as "n"
2. if n is even, divide it by 2. if n is odd, replace it with 3n + 1
3. repeat step 2 until n = 1.

Read more @ https://en.wikipedia.org/wiki/Collatz_conjecture

### Content ###

This repository contains Collatz implementations in different languages.

Currently contains:

* C++ implementation

* C# implementation

* Java implementation

* Python implementation